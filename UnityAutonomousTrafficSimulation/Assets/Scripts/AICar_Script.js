
// These variables allow the script to power the wheels of the car.
var FrontLeftWheel : WheelCollider;
var FrontRightWheel : WheelCollider;
var RearLeftWheel : WheelCollider;
var RearRightWheel : WheelCollider;

var mphDisplay : GUIText;

var vehicleCenterOfMass : float = 0.0;
var steeringSharpness : float = 12.0;
var LastGPSPosition : Vector3 ;



//GPS parameters
var GPSaccuracy : float = 0.0;
var CursLength : float = 0.0;
var q_car : float = 0.0;



// These variables are for the gears, the array is the list of ratios. The script
// uses the defined gear ratios to determine how much torque to apply to the wheels.
var GearRatio : float[];
var CurrentGear : int = 0;

////////////////////////////////////////////////////////////////////////////////////////////////////////////
var wheelAngle : float = 0;

// These variables are just for applying torque to the wheels and shifting gears.
// using the defined Max and Min Engine RPM, the script can determine what gear the
// car needs to be in.
var EngineTorque : float = 600.0;
var BrakePower : float = 0;
var MaxEngineRPM : float = 3000.0;
var MinEngineRPM : float = 1000.0;
private var EngineRPM : float = 0.0;

// Here's all the variables for the AI, the waypoints are determined in the "GetWaypoints" function.
// the waypoint container is used to search for all the waypoints in the scene, and the current
// waypoint is used to determine which waypoint in the array the car is aiming for.
var waypointContainer : GameObject;
private var waypoints : Array;
private var currentWaypoint : int = 0;
private var nextWaypoint : int = 0;
private var prevtWaypoint : int = 0;

// input steer and input torque are the values substituted out for the player input. The 
// "NavigateTowardsWaypoint" function determines values to use for these variables to move the car
// in the desired direction.
private var inputSteer : float = 0.0;
private var inputTorque : float = 0.0;

function Start () {
	// I usually alter the center of mass to make the car more stable. I'ts less likely to flip this way.
	rigidbody.centerOfMass.y = (vehicleCenterOfMass);
	LastGPSPosition = transform.position;
	
	// Call the function to determine the array of waypoints. This sets up the array of points by finding
	// transform components inside of a source container.
	GetWaypoints();
	q_car = Quaternion.LookRotation( rigidbody.transform.forward).eulerAngles.y ;
}

function FixedUpdate () {


	var mph = rigidbody.velocity.magnitude * 2.237;
	mphDisplay.text = mph.ToString("F0") + " : MPH"; // displays one digit after the dot
	
	// This is to limith the maximum speed of the car, adjusting the drag probably isn't the best way of doing it,
	// but it's easy, and it doesn't interfere with the physics processing.
	rigidbody.drag = rigidbody.velocity.magnitude / 250;
	
	// Call the funtion to determine the desired input values for the car. This essentially steers and
	// applies gas to the engine.
	NavigateTowardsWaypoint();
	
	// Compute the engine RPM based on the average RPM of the two wheels, then call the shift gear function
	EngineRPM = (FrontLeftWheel.rpm + FrontRightWheel.rpm)/2 * GearRatio[CurrentGear];
	ShiftGears();

	// set the audio pitch to the percentage of RPM to the maximum RPM plus one, this makes the sound play
	// up to twice it's pitch, where it will suddenly drop when it switches gears.
	audio.pitch = Mathf.Abs(EngineRPM / MaxEngineRPM) + 1.0 ;
	// this line is just to ensure that the pitch does not reach a value higher than is desired.
	if ( audio.pitch > 2.0 ) {
		audio.pitch = 2.0;
	}
	
	// finally, apply the values to the wheels.	The torque applied is divided by the current gear, and
	// multiplied by the calculated AI input variable.
	FrontLeftWheel.motorTorque = EngineTorque / GearRatio[CurrentGear] * inputTorque;
	FrontRightWheel.motorTorque = EngineTorque / GearRatio[CurrentGear] * inputTorque;
	
	RearLeftWheel.brakeTorque = BrakePower;
	RearRightWheel.brakeTorque = BrakePower;
		
	// the steer angle is an arbitrary value multiplied by the calculated AI input.
	
	if(wheelAngle>0)
	{
		FrontLeftWheel.steerAngle = 0.9302f * wheelAngle;//(steeringSharpness) * inputSteer;
		FrontRightWheel.steerAngle = wheelAngle;//(steeringSharpness) * inputSteer;
	}
	else{
		FrontLeftWheel.steerAngle =  wheelAngle;//(steeringSharpness) * inputSteer;
	    FrontRightWheel.steerAngle = 0.9302f * wheelAngle;//(steeringSharpness) * inputSteer;
	
	}
}

function ShiftGears() {
	// this funciton shifts the gears of the vehcile, it loops through all the gears, checking which will make
	// the engine RPM fall within the desired range. The gear is then set to this "appropriate" value.
	if ( EngineRPM >= MaxEngineRPM ) {
		var AppropriateGear : int = CurrentGear;
		
		for ( var i = 0; i < GearRatio.length; i ++ ) {
			if ( FrontLeftWheel.rpm * GearRatio[i] < MaxEngineRPM ) {
				AppropriateGear = i;
				break;
			}
		}
		
		CurrentGear = AppropriateGear;
	}
	
	if ( EngineRPM <= MinEngineRPM ) {
		AppropriateGear = CurrentGear;
		
		for ( var j = GearRatio.length-1; j >= 0; j -- ) {
			if ( FrontLeftWheel.rpm * GearRatio[j] > MinEngineRPM ) {
				AppropriateGear = j;
				break;
			}
		}
		
		CurrentGear = AppropriateGear;
	}
}

function GetWaypoints () {
	// Now, this function basically takes the container object for the waypoints, then finds all of the transforms in it,
	// once it has the transforms, it checks to make sure it's not the container, and adds them to the array of waypoints.
	/*var potentialWaypoints : Array = waypointContainer.GetComponentsInChildren( Transform );
	waypoints = new Array();
	
	for ( var potentialWaypoint : Transform in potentialWaypoints ) {
		if ( potentialWaypoint != waypointContainer.transform ) {
			waypoints[ waypoints.length ] = potentialWaypoint;
		}
	}*/
	var script = waypointContainer.GetComponent("Spline");
         
	waypoints= script.spline;
}

function OnDrawGizmos()
{

	
	//var point1 = transform.position;

// var point2 = GetDist(waypoints[prevWaypoint].transform.position,waypoints[currentWaypoint].transform.position-waypoints[prevWaypoint].transform.position, transform.position);
//Gizmos.color = Color.blue;
//Gizmos.DrawLine (point1, point2);

}

function NavigateTowardsWaypoint () {


	



   GetWaypoints();
   
   
			
    var nextWaypoint = currentWaypoint + 1;	
		if ( nextWaypoint == waypoints.length) {
				nextWaypoint = 0;
			}
	// now we just find the relative position of the waypoint from the car transform,
	// that way we can determine how far to the left and right the waypoint is.
	var RelativeWaypointPosition : Vector3 = transform.InverseTransformPoint( Vector3( 
												waypoints[currentWaypoint].position.x, 
												transform.position.y, 
												waypoints[currentWaypoint].position.z ) );
																																																																																																																																									
	// by dividing the horizontal position by the magnitude, we get a decimal percentage of the turn angle that we can use to drive the wheels
	inputSteer = RelativeWaypointPosition.x / RelativeWaypointPosition.magnitude;
	
	var prevWaypoint = currentWaypoint - 1;	
	if ( prevWaypoint == -1 ) {
			prevWaypoint = waypoints.length-1;
		}
	  
	 var q =  Quaternion.LookRotation(waypoints[currentWaypoint].transform.position-waypoints[prevWaypoint].transform.position).eulerAngles.y;
	
	
	//Здесь добавить неточное вращение
	//для курса авто
	
	var gps_position = transform.position;
	gps_position.x+=Random.Range(-GPSaccuracy/2, GPSaccuracy/2);
	gps_position.z+=Random.Range(-GPSaccuracy/2, GPSaccuracy/2);
	
	
	
	
	if((gps_position - LastGPSPosition).magnitude>CursLength)
	{
		q_car = Quaternion.LookRotation((gps_position- LastGPSPosition),Vector3.up).eulerAngles.y;
		LastGPSPosition = gps_position;
	}
	
	 
	 var angleDelta=q-q_car;
	 
	 if( angleDelta < -180 ){
    	angleDelta = angleDelta + 2*180;
     }
	 else if( angleDelta > 180 ){
    	angleDelta = angleDelta - 2*180;
     }
 	 
	
	 var psy =  angleDelta;
	 
	 
	var a1=waypoints[prevWaypoint].position;
	var b=waypoints[currentWaypoint].position;
	var c=rigidbody.transform.position;
	
	 
	var dist_error=(GetDist(waypoints[prevWaypoint].position, waypoints[currentWaypoint].position-waypoints[prevWaypoint].position, gps_position )-gps_position).magnitude;
    
    if (((b.x - a1.x)*(c.z - a1.z) - (b.z - a1.z)*(c.x - a1.x)) < 0 ){		   
		    dist_error=-dist_error;
		    psy=psy;   
    }
	 
	  
	 //var a = psy - Mathf.Rad2Deg * Mathf.Atan2(0.06 * dist_error  ,rigidbody.velocity.magnitude);
	var a = psy + Mathf.Rad2Deg * Mathf.Atan2(0.4 * dist_error , rigidbody.velocity.magnitude);
	wheelAngle = a;
	
	

	mphDisplay.text = wheelAngle.ToString("F1") + " : Wheel angle "+(dist_error).ToString("F0")+" : DistError; "+(psy).ToString("F0")+" : Psy; ";	
	
	if(wheelAngle>33){
		wheelAngle=33;
	}
	if(wheelAngle<-33){
		wheelAngle=-33;
	}
	GetComponent("InnertialPositionCalculator").SendMessage("receiveAngle", (wheelAngle+0.9302f * wheelAngle)/2);
	// now we do the same for torque, but make sure that it doesn't apply any engine torque when going around a sharp turn...
	
	
	if ( Mathf.Abs( Mathf.Abs(wheelAngle/15) ) < 0.5 ) {
		inputTorque = 0.5*RelativeWaypointPosition.z / RelativeWaypointPosition.magnitude - Mathf.Abs( inputSteer );
		
	}else{
		inputTorque=1;
	}
	
	
	
	
	// this just checks if the car's position is near enough to a waypoint to count as passing it, if it is, then change the target waypoint to the
	// next in the list.
	if ( RelativeWaypointPosition.magnitude < 3 ) {
		currentWaypoint ++;
		
		if ( currentWaypoint >= waypoints.length ) {
			currentWaypoint = 0;
		}
	}
	
	if(rigidbody.velocity.magnitude>2)
	{
			inputTorque=0;
			BrakePower=3;
	
	}
	
}

function GetDist(  origin: Vector3,  direction: Vector3,  point: Vector3) {
		direction.Normalize();

		var v= point - origin;
		var d = Vector3.Dot(v, direction);

		return  origin + direction*d;
}