﻿using UnityEngine;
using System.Collections;
using System;

public class InnertialNaviServer {
	
	//Car ground speed
	private float speed=0;
	
	public InnertialNaviServer(float speed, float x, float y, float carAngle, float base_of_car) {
		
		this.speed=speed;
		this.x=x;
		this.y=y;
		this.carAngle=carAngle;
		
		this.base_of_car=base_of_car;
		lastTime = DateTime.Now.Millisecond;
		
	}
	//Car x pos
	private float x=0;
	//Car y pos
	private float y=0;
	
	private float carAngle=0;
	
	private float x_speed=0, y_speed=0, carAngle_Speed=0;
	
	private float base_of_car=3;
	
	public float ValidateAngle(float angle)
	{
		if(angle > 360)
		{
			angle -= 180*2;
		}
		if(angle<0)
		{
			angle += 180*2;
		}
		
		return angle;
	}
	public Rigidbody rb;
	long lastTime=0;


	public void Update (float dt, float angle, float angularVelocity) {


		//Угловая скорость
		carAngle_Speed = (float)((speed/base_of_car)* Mathf.Tan( wheelAngle*Mathf.Deg2Rad ));


		Debug.Log ( "car angle speed: " + (carAngle_Speed).ToString () + "angular velocity: " + (angularVelocity).ToString () );

		carAngle_Speed = carAngle_Speed*Mathf.Rad2Deg; 				 
		carAngle = ( carAngle + carAngle_Speed * dt );	
		carAngle = ValidateAngle(carAngle);


		//Debug.Log ("dt: " + dt.ToString());
		 
		//Изменение координат
		x_speed = speed * Mathf.Cos( angle * Mathf.Deg2Rad );
		y_speed = speed * Mathf.Sin( angle * Mathf.Deg2Rad );
		
		x +=x_speed * dt;
		y +=y_speed * dt;
		
	}
	
	public void CalculateNavigation(float wheel_angle, float speed, float delta_time)
	{
		this.wheelAngle = wheel_angle;
		this.speed = speed;		 
	}
		
	public float getCarAngle() {
		return carAngle;
	}
	
	public void setCarAngle(float carAngle) {
		this.carAngle = carAngle;
	}
	
	public float getX_speed() {
		return x_speed;
	}
	
	public void setX_speed(float x_speed) {
		this.x_speed = x_speed;
	}
	
	public float getY_speed() {
		return y_speed;
	}
	
	public void setY_speed(float y_speed) {
		this.y_speed = y_speed;
	}
	
	public float getCarAngle_Speed() {
		return carAngle_Speed;
	}
	
	public void setCarAngle_Speed(float carAngle_Speed) {
		this.carAngle_Speed = carAngle_Speed;
	}
	
	//wheel angle
	private float wheelAngle=0;
	
	public float getWheelAngle() {
		return wheelAngle;
	}
	
	public void setWheelAngle(float wheelAngle) {
		this.wheelAngle = wheelAngle;
	}
	
	public float getX() {
		return x;
	}
	
	public void setX(float x) {
		this.x = x;
	}
	
	public float getY() {
		return y;
	}
	
	public void setY(float y) {
		this.y = y;
	}
	
	public float getSpeed() {
		return speed;
	}
	 
	public void setSpeed(float speed) {
		this.speed = speed;
	}   
}

