﻿using UnityEngine;
using System.Collections;

public class InnertialPositionCalculator : MonoBehaviour {


	private float wheel_angle=0;
	InnertialNaviServer inv = null;
	public Rigidbody rb;
	// Use this for initialization
	void Start () {
		InvokeRepeating("Calc", 0.0f, 0.1f);

		//InnertialNaviServer(float speed, float x, float y, float carAngle, float base_of_car)
		rb = GetComponent<Rigidbody>();
		inv = new InnertialNaviServer (0.0f, -238.9f ,   -153.8f, 270, 3.665421f);
		inv.rb = rb;
	}
	void Update()
	{
		var fwdSpeed = Vector3.Dot(rigidbody.velocity, transform.forward.normalized);
		inv.CalculateNavigation( wheel_angle, fwdSpeed, 0.1f);
		inv.Update (Time.deltaTime, transform.rotation.eulerAngles.y, rigidbody.angularVelocity.y);

    }

	// Update is called once per frame
	void Calc () {

		Debug.Log ("Transform x:"+(rigidbody.transform.position.x ).ToString()+ " Transform car x:"+ inv.getY().ToString()+ 
		           " Car angle: "+(inv.getCarAngle() ).ToString()+" Car system angle" 
		           + rigidbody.transform.rotation.eulerAngles.y);

		Debug.Log ("Transform z:"+(rigidbody.transform.position.z ).ToString()+ " Transform car z:"+ inv.getX().ToString()
		           );

	}
	
	void receiveAngle(float value)
	{
		this.wheel_angle = value;
		//Debug.Log (value);
    }
}
