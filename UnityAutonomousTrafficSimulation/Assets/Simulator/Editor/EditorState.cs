﻿using UnityEngine;
using System.Collections;

public abstract class EditorState  {

	 

	public abstract void insideSceneGUI(RoadSystem roadSytem, RoadSystemEditor editor);

	public abstract void OnSceneGUI (RoadSystem target, RoadSystemEditor editor) ;

}
