﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System;
using System.Reflection;
using System.Collections.Generic;

public class DefaultHandles
{
	public static bool Hidden {
		get {
			Type type = typeof(Tools);
			FieldInfo field = type.GetField ("s_Hidden", BindingFlags.NonPublic | BindingFlags.Static);
			return ((bool)field.GetValue (null));
		}
		set {
			Type type = typeof(Tools);
			FieldInfo field = type.GetField ("s_Hidden", BindingFlags.NonPublic | BindingFlags.Static);
			field.SetValue (null, value);
		}
	}
}

public enum ModeOfEditor
{		 
	EditRoads,
	EditNodes,
	EditNodeLine
}

[CustomEditor(typeof(RoadSystem))]
public class RoadSystemEditor : Editor
{
    private int needAddspline =0;
    private bool hideDefaultHandle = true;
	EditorState state = null;
    private Vector3 lastPicked = new Vector3 (0, 0, 0);
	ModeOfEditor modeEditor = ModeOfEditor.EditRoads;
    public EditNodesState NodesState { get; set; }
    public EditNodeLineState EditNodesLinesState { get; set; }
    public EditRoadesState EditRoadsState { get; set; }
    private Road splinePt1=null;

    private Road splinePt2=null;
    private SplineLine activeTaggetSpline = null;
    public Intersection ActiveTargetJunction { get; set; }
    float numRoads = 0;

    public int NeedAddspline
    {
        get { return needAddspline; }
        set { needAddspline = value; }
    }

    public bool HideDefaultHandle
    {
        get { return hideDefaultHandle; }
        set { hideDefaultHandle = value; }
    }

    public EditorState State
    {
        get { return state; }
        set { state = value; }
    }

    public Vector3 LastPicked
    {
        get { return lastPicked; }
        set { lastPicked = value; }
    }

    public ModeOfEditor ModeEditor
    {
        get { return modeEditor; }
        set { modeEditor = value; }
    }

    public Road SplinePt1
    {
        get { return splinePt1; }
        set { splinePt1 = value; }
    }

    public Road SplinePt2
    {
        get { return splinePt2; }
        set { splinePt2 = value; }
    }

    public SplineLine ActiveTaggetSpline
    {
        get { return activeTaggetSpline; }
        set { activeTaggetSpline = value; }
    }

    public float NumRoads
    {
        get { return numRoads; }
        set { numRoads = value; }
    }


    EditorState CreateState (ModeOfEditor state)
	{
		switch (state) {

		 
		case ModeOfEditor.EditNodeLine:
			return EditNodesLinesState;
		case ModeOfEditor.EditNodes:
			return NodesState;

		case ModeOfEditor.EditRoads:
			return EditRoadsState;		 
		}
		return null;

	}

	void ChangeState (ModeOfEditor stateMode)
	{
		this.ModeEditor = stateMode;
		State = CreateState (ModeEditor);
	}

	void OnEnable ()
	{
		DefaultHandles.Hidden = HideDefaultHandle;
		State = new EditRoadesState ();
		NodesState = new EditNodesState ();
		EditNodesLinesState = new EditNodeLineState ();
		EditRoadsState = new EditRoadesState ();
	}
	
	void OnDisable ()
	{
		DefaultHandles.Hidden = false;
	}
	 
	public int selGridInt = 0;
	public Rect rc ;
	public float sizeButton = 30;
	public Rect size ;
	public bool addRoadFlag = false;
	public void DestroyObject (UnityEngine.Object obj)
	{

		DestroyImmediate (obj);
	}

	void preRender ()
	{
		size = new Rect (0, 0, 400, 300);
		  
		Handles.BeginGUI ();
		//  Handles.BeginGUI(new Rect(Screen.width - size.width - 10, Screen.height - size.height - 10, size.width, size.height));
		
		GUI.BeginGroup (new Rect (Screen.width - size.width - 10, Screen.height - size.height - 50, size.width, size.height));
		GUI.Box (size, "Road System Tool Bar");
		rc = new Rect (0, sizeButton, size.width, sizeButton);

		rc.width = size.width / 3;
		string[] selStrings = new string[] {"EditRoads", "EditNodes", "EditSplines"};
		
		selGridInt = GUI.SelectionGrid (new Rect (25, 25, 300, 50), selGridInt, selStrings, 3);
		
		if (ModeEditor != (ModeOfEditor)selGridInt || State == null) {
			ModeEditor = (ModeOfEditor)selGridInt;
			
			
			ChangeState (ModeEditor);
		}
		rc.y += sizeButton * 2;
		GUI.Label (rc, "Double Clic on Circles to select a point");

	    

    }

	void insideSceneGUI (RoadSystem roadSytem)
	{
		preRender ();		 
		State.insideSceneGUI (roadSytem, this);
		postRender ();
	  //  Graph g = roadSytem.getGraph();
	}

	void postRender ()
	{
		GUI.EndGroup ();
		Handles.EndGUI ();
	}

	
	void OnSceneGUI ()
	{
		RoadSystem roadSystem = (RoadSystem)target;
		State.OnSceneGUI (roadSystem, this);
		insideSceneGUI (roadSystem);
	}
	
	override public void  OnInspectorGUI ()
	{
		base.OnInspectorGUI ();
	}
}
