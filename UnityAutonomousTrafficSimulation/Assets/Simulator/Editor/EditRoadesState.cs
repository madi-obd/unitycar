//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.42000
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using UnityEditor;
using UnityEngine;
 
public class EditRoadesState:EditorState
{
	public EditRoadesState ()
	{
	}

	Vector3[] globalPos;
	Vector3 lastPicked;
	int curPointIndex = 0;
	Road activeTarget = null;
	bool moveMulti;
	public override void OnSceneGUI (RoadSystem target, RoadSystemEditor editor)
	{


		Event e1 = Event.current;
		if(EventType.KeyDown == e1.type && KeyCode.Space == e1.keyCode)
		{
    			moveMulti = true;
		}
		
		if(EventType.KeyUp == e1.type && KeyCode.Space == e1.keyCode)
		{
			moveMulti = false;
		}
		//Handles.BeginGUI();
		// Thank to Dantus from Edelweiss for this helper
		// You need them somewhere.
		Quaternion handleRotation = Quaternion.identity;
		Vector3 mousePosition = Event.current.mousePosition;
		mousePosition.y = SceneView.currentDrawingSceneView.camera.pixelHeight - mousePosition.y;
		mousePosition = SceneView.currentDrawingSceneView.camera.ScreenToWorldPoint (mousePosition);
		mousePosition.y = -mousePosition.y;
		RoadSystem roadSystem = (RoadSystem)target;
		Transform tr = roadSystem.transform;


		for (int k=0; k<roadSystem.roads.Count; k++) {
			Road road = roadSystem.roads [k];
						
			if (road.Positions == null) {
				continue;
			}

			globalPos = new Vector3[road.Positions.Length];
			float size = 0;
			for (int i = 0; i < road.Positions.Length; i++) {   
				
				Vector3 pos = tr.TransformPoint (road.Positions [i]);
				size = HandleUtility.GetHandleSize (pos);
				Event e = Event.current;
				
				Handles.Label (pos, road.GetHashCode () + " x= " + pos.x + " z= " + pos.z + "   " + i);
				int someHashCode = road.Positions.GetHashCode ();
				
				// Get the needed data before the handle
				int controlIDBeforeHandle = GUIUtility.GetControlID (someHashCode, FocusType.Passive);
				bool isEventUsedBeforeHandle = (Event.current.type == EventType.used);
				
				//
				// pos = Handles.PositionHandle(pos, Quaternion.identity);
				
				Handles.ScaleValueHandle (0, pos, Quaternion.identity, size, Handles.SphereCap, 0);
				
				//Debug.Log(GUIUtility.hotControl.GetHashCode());
				if (Event.current.type == EventType.MouseMove  && moveMulti) {
					
					Ray ray1 = HandleUtility.GUIPointToWorldRay (Event.current.mousePosition);
					
					
					lastPicked = new Vector3 (ray1.origin.x, 0, ray1.origin.z);
					
					//Debug.Log ("Size=" + size + " ray x = " + ray1.origin.x + " ray z " + ray1.origin.z);
					SphereCollider sk = roadSystem.gameObject.AddComponent<SphereCollider> ();
					sk.center = pos;
					
					sk.radius = 2;
					editor.LastPicked = new Vector3 (ray1.origin.x, 0, ray1.origin.z);
					RaycastHit hit = new RaycastHit ();
					if (/*sk.Raycast(ray1, out hit, 10000.0f) */ Math.Abs (ray1.origin.x - pos.x) < sk.radius && Math.Abs (ray1.origin.z - pos.z) < sk.radius) {
						var allColliders = roadSystem.gameObject.GetComponents<Collider> ();
						
						foreach (var childCollider in allColliders)
							editor.DestroyObject (childCollider);
						
						Debug.Log (" i = " + i + " Hitted point x=" + hit.point.x + "  z= " + hit.point.z + " radius = " + size + " target = "
							+ road.GetHashCode ());

						activeTarget = road;

						curPointIndex = i;

						if (editor.addRoadFlag) {
							editor.ActiveTargetJunction.addRoad (activeTarget, curPointIndex);
							editor.ActiveTargetJunction.connectIndixes.Add (i);
							editor.addRoadFlag = false;
						}
						if (editor.NeedAddspline == 2) {
							editor.NeedAddspline--;
							editor.SplinePt1 = activeTarget;
						} else {
							if (editor.NeedAddspline == 1 && activeTarget != editor.SplinePt1) {
								
								editor.NeedAddspline--;
								editor.SplinePt2 = activeTarget;
								SplineLine pl = new GameObject ().AddComponent<SplineLine> ();

								pl.controlPointsList = new Vector3[7];
								
                    			Vector3 a = editor.SplinePt1.Positions [editor.ActiveTargetJunction.getBundleByRoad (editor.SplinePt1).LineNumberAtRoad>0?editor.SplinePt1.Positions.Length-1:0];
								
								Vector3 b = editor.SplinePt2.Positions [editor.ActiveTargetJunction.getBundleByRoad (editor.SplinePt2).LineNumberAtRoad>0?editor.SplinePt2.Positions.Length-1:0];

								pl.controlPointsList [0] = tr.TransformPoint (a);
								pl.controlPointsList [1] = tr.TransformPoint (a);
								int bt = 1;
								for (float m =0.0f; m < 1.1f; m = m+0.25f) {
									pl.controlPointsList [bt] = tr.TransformPoint (a * (1 - m) + b * (m));
									bt++;
									
								}
								pl.controlPointsList [5] = tr.TransformPoint (b);
								pl.controlPointsList [6] = tr.TransformPoint (b);
								pl.BeginRoad = editor.SplinePt1;
								pl.EndRoad = editor.SplinePt2;
								editor.ActiveTargetJunction.splineLines.Add (pl);
								
							}
						}
					} else {
						var allColliders1 = roadSystem.gameObject.GetComponents<Collider> ();
						foreach (var childCollider in allColliders1)
							editor.DestroyObject (childCollider);
						//continue;
					}												
				}
				 
				if (curPointIndex == i && activeTarget == road) {
					Vector3 pos1 = Handles.PositionHandle (pos, Quaternion.identity);
					if (pos1 != pos) {
						foreach(var junc in target.junctions)
						{
							foreach(var spl in junc.splineLines)
							{
								spl.update();
							}

						}
						pos = pos1;
					}
				}

				// Get the needed data after the handle
				int controlIDAfterHandle = GUIUtility.GetControlID (someHashCode, FocusType.Passive);
				bool isEventUsedByHandle = !isEventUsedBeforeHandle && (Event.current.type == EventType.used);
				
				if
					(((controlIDBeforeHandle < GUIUtility.hotControl &&
					GUIUtility.hotControl < controlIDAfterHandle) ||
					isEventUsedByHandle)&&moveMulti) {
					curPointIndex = i;
				}
				
				road.Positions [i] = tr.InverseTransformPoint (pos);
				globalPos [i] = pos;
			}
			
			Handles.DrawPolyLine (globalPos);
			Handles.color = Color.red;
			if (road.GetLine (1, globalPos) != null) {
				Handles.DrawPolyLine (road.GetLine (1, globalPos));
				Vector3[] cups = road.GetLine (1, globalPos);
				Handles.color = Color.green;
				Handles.ScaleValueHandle (0, cups [0], Quaternion.identity, size, Handles.SphereCap, 0);
				Handles.color = Color.red;
				Handles.ScaleValueHandle (0, cups [cups.Length - 1], Quaternion.identity, size, Handles.SphereCap, 0);
				Handles.color = Color.white;
				Handles.DrawPolyLine (road.GetLine (-1, globalPos));
				
				cups = road.GetLine (-1, globalPos);
				Handles.color = Color.green;
				Handles.ScaleValueHandle (0, cups [0], Quaternion.identity, size, Handles.SphereCap, 0);
				Handles.color = Color.red;
				Handles.ScaleValueHandle (0, cups [cups.Length - 1], Quaternion.identity, size, Handles.SphereCap, 0);
			}
		}
	}

	public override void insideSceneGUI (RoadSystem roadSytem, RoadSystemEditor editor)
	{
		if (GUI.Button (editor.rc, "Add Road")) {
			
			Road road = new GameObject ().AddComponent<Road> ();
			
            road.setBeginNode(new RoadNode());
            road.setEndNode(new RoadNode());
			road.Positions = new Vector3[2];
			if (roadSytem.roads.Count == 0) {
				road.Positions [0] = roadSytem.transform.position;
				road.Positions [1] = roadSytem.transform.position + new Vector3 (10, 0, 10);
				road.MainTransform = roadSytem.transform;
				road.LineNumber = 1;
			} else {
				road.Positions [0] = roadSytem.roads [roadSytem.roads.Count - 1].Positions [0] + new Vector3 (10, 0, 10);
				road.Positions [1] = roadSytem.roads [roadSytem.roads.Count - 1].Positions [1] + new Vector3 (10, 0, 10);
				road.MainTransform = roadSytem.transform;
				road.LineNumber = 1;
			}
			roadSytem.roads.Add (road);
		}
		editor.rc.x += editor.rc.width;
		if (GUI.Button (editor.rc, "Add Point")) {
			Vector3[] positionsOfRoad = new Vector3[activeTarget.Positions.Length+1];
			 

			for(int i = 0 ; i<activeTarget.Positions.Length+1; i++)
			{
				if( i == curPointIndex )
				{
					positionsOfRoad[i+1] = new Vector3(activeTarget.Positions[i].x, activeTarget.Positions[i].y, activeTarget.Positions[i].z)  ;
					positionsOfRoad[i] = new Vector3(activeTarget.Positions[i].x, activeTarget.Positions[i].y, activeTarget.Positions[i].z) ;
				}
				if(i>curPointIndex)
				{
					positionsOfRoad[i] = activeTarget.Positions[i-1];
				}
				else if(i<curPointIndex){
					positionsOfRoad[i] = activeTarget.Positions[i];
				}
			}
			activeTarget.Positions = positionsOfRoad;

		}
		editor.rc.x += editor.rc.width;
 
		editor.rc.y += editor.sizeButton + 10;
		editor.rc.x = 0;
		editor.rc.width = editor.size.width;
		if (editor.HideDefaultHandle) {
			if (GUI.Button (editor.rc, "Show Main Transform")) {
				editor.HideDefaultHandle = false;
				DefaultHandles.Hidden = editor.HideDefaultHandle;
			}
		} else {
			if (GUI.Button (editor.rc, "Hide Main Transform")) {
				editor.HideDefaultHandle = true;
				DefaultHandles.Hidden = editor.HideDefaultHandle;
			}
		}
		editor.rc.y += editor.sizeButton + 10;
		editor.rc.x = 0;

        if (GUI.Button(editor.rc, "Delete current road"))
        {
            if(activeTarget!=null)
            {

                roadSytem.DeleteRoad(activeTarget);
                activeTarget = null;

            }
        }
        editor.rc.y += editor.sizeButton + 10;
        editor.rc.x = 0;
        //numRoads = GUI.HorizontalSlider (rc, numRoads, 0, 10);
    }
}
 

