﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class Spline : MonoBehaviour {

     public GameObject waypointContainer =null;
	 public Transform[] spline = null;


	public void Start()
	{

		var waypoints = GetWaypoints ();
		var vectors = new Vector2[waypoints.Length];
		Debug.Log (vectors.Length);
		for(var d=0; d<waypoints.Length; d++)
		{			
			vectors[d].x=waypoints[d] .x;
			vectors[d].y=waypoints[d] .z;			
		}
		
		var spline =  Calculate(vectors, 2, 5, true);
		this.spline =  CalculateTransform(vectors, 2, 5, true);
	}
	/// <summary>
	/// Вычисляет узловые точки дискретного N-периодического сплайна с векторными коэфициентами.
	/// </summary>
	/// <param name="aPoints">Полюса сплайна (исходные точки). Должно быть не менее 2-х полюсов.</param>
	/// <param name="r">Порядок сплайна.</param>
	/// <param name="n">Число узлов между полюсами сплайна.</param>
	/// <param name="aIsIncludeOriginalPoints">True - сплайн будет проходить через полюса, false - сплайн не будет проходить через полюса.</param>
	/// <returns></returns>
	public static Vector2[] Calculate(Vector2[] aPoints, int r, int n , bool aIsIncludeOriginalPoints) {
		if (aPoints == null) {
			throw new ArgumentNullException("aPoints");
		}
		
		if (aPoints.Length <= 2) {
			throw new ArgumentException("Число полюсов должно быть > 2.");
		}
		
		if (r <= 0) {
			throw new ArgumentException("Порядок сплайна должен быть > 0.");
		}
		
		if (n < 1) {
			throw new ArgumentException("Число узлов между полюсами сплайна должно быть >= 1.");
		}
		
		var m = aPoints.Length;
		var N = n * m;
		
		Vector2[] vectors;
		if (aIsIncludeOriginalPoints) {
			vectors = RecalculateVectors(aPoints, r, n, m);
		} else {
			vectors = new Vector2[m];
			aPoints.CopyTo(vectors, 0);
		}
		
		var qSpline = CalculateQSpline(n, m);
		var resultPoints = CalculateSSpline(vectors, qSpline, r, n, m);
		
		return resultPoints;
	}
	public static Transform[] CalculateTransform(Vector2[] aPoints, int r, int n , bool aIsIncludeOriginalPoints) {
		if (aPoints == null) {
			throw new ArgumentNullException("aPoints");
		}
		
		if (aPoints.Length <= 2) {
			throw new ArgumentException("Число полюсов должно быть > 2.");
		}
		
		if (r <= 0) {
			throw new ArgumentException("Порядок сплайна должен быть > 0.");
		}
		
		if (n < 1) {
			throw new ArgumentException("Число узлов между полюсами сплайна должно быть >= 1.");
		}
		
		var m = aPoints.Length;
		var N = n * m;
		
		Vector2[] vectors;
		if (aIsIncludeOriginalPoints) {
			vectors = RecalculateVectors(aPoints, r, n, m);
		} else {
			vectors = new Vector2[m];
			aPoints.CopyTo(vectors, 0);
		}
		
		var qSpline = CalculateQSpline(n, m);
		var resultPoints = CalculateSSpline(vectors, qSpline, r, n, m);
		
		
		Transform[] result = new Transform[resultPoints.Length];
		

		for (var i = 0; i < result.Length;  i++) {
			result[i]= new GameObject().transform;
			result[i].position=new Vector3(resultPoints[i].x, 0.0f, resultPoints[i].y);

		}
		
		return result;
	}
	
	/// <summary>
	/// Вычисляет вектора дискретного периодического сплайна с векторными коэфициентами, согласно
	/// формулам http://www.math.spbu.ru/ru/mmeh/AspDok/pub/2010/Chashnikov.pdf (страница 7).
	/// </summary>
	/// <param name="vectors"></param>
	/// <param name="qSpline"></param>
	/// <param name="r"></param>
	/// <param name="n"></param>
	/// <param name="m"></param>
	/// <returns></returns>
	private static Vector2[] CalculateSSpline(Vector2[] aVectors, float[] aQSpline, int r, int n, int m) {            
		var N = n * m;
		var sSpline = new Vector2[r + 1][];
		for (var i = 1; i <= r; ++i) {
			sSpline[i] = new Vector2[N];
		      
		}
		
		for (var j = 0; j < N; ++j) {
			sSpline[1][j] = new Vector2(0, 0);
			for (var p = 0; p < m; ++p) {
				sSpline[1][j] += aVectors[p] * aQSpline[GetPositiveIndex(j - p * n, N)];
			}
		}
		
		for (var v = 2; v <= r; ++v) {
			for (var j = 0; j < N; ++j) {
				sSpline[v][j] = new Vector2(0, 0);
				for (var k = 0; k < N; ++k) {
					sSpline[v][j] += aQSpline[k] * sSpline[v - 1][GetPositiveIndex(j - k, N)];
				}
				sSpline[v][j] /= n;
			}
		}
		
		return sSpline[r];
	}        
	
	/// <summary>
	/// Вычисляет коэфициенты дискретного периодического Q-сплайна 1-ого порядка, согдасно 
	/// формулам http://www.math.spbu.ru/ru/mmeh/AspDok/pub/2010/Chashnikov.pdf (страница 6).
	/// </summary>
	/// <param name="n">Число узлов между полюсами.</param>
	/// <param name="m">Число полюсов.</param>
	/// <returns>Коэфициенты дискретного периодического Q-сплайна 1-ого порядка.</returns>
	private static float[] CalculateQSpline(int n, int m) {
		var N = n * m;
		var qSpline = new float[N];
		
		for (var j = 0; j < N; ++j) {
			if (j >= 0 && j <= n - 1) {
				qSpline[j] = (1.0f * n - j) / n;
			}
			if (j >= n && j <= N - n) {
				qSpline[j] = 0;
			}
			if (j >= N - n + 1 && j <= N - 1) {
				qSpline[j] = (1.0f * j - N + n) / n;
			}
		}
		
		return qSpline;
	}
	
	/// <summary>
	/// Пересчитывает коэфициенты сплайна для того, чтобы результирующий сплайн проходил через полюса.
	/// http://dha.spb.ru/PDF/discreteSplines.pdf (страница 6 и 7). 
	/// </summary>
	/// <param name="aPoints">Исходные точки.</param>
	/// <param name="r">Порядок сплайна.</param>
	/// <param name="n">Количество узлов между полюсами сплайна.</param>
	/// <param name="m">Количество полюсов.</param>
	/// <returns></returns>
	private static Vector2[] RecalculateVectors(Vector2[] aPoints, int r, int n, int m) {
		var N = n * m;
		
		// Вычисляем знаменатель.
		var tr = new float[m];
		tr[0] = 1;            
		for (var k = 1; k < m; ++k) {
			for (var q = 0; q < n; ++q) {
				tr[k] += Mathf.Pow(2 * n * Mathf.Sin((Mathf.PI * (q * m + k)) / N), -2 * r);
			}
			tr[k] *= Mathf.Pow(2 * Mathf.Sin((Mathf.PI * k) / m), 2 * r);
		}
		
		// Вычисляем числитель.
		var zre = new Vector2[m];
		var zim = new Vector2[m];
		for (var j = 0; j < m; ++j) {
			zre[j] = new Vector2(0, 0);
			zim[j] = new Vector2(0, 0);
			for (var k = 0; k < m; ++k) {
				zre[j] += aPoints[k] * Mathf.Cos((-2 * Mathf.PI * j * k) / m);
				zim[j] += aPoints[k] * Mathf.Sin((-2 * Mathf.PI * j * k) / m);
			}
		}
		
		// Считаем результат.
		var result = new Vector2[m];
		for (var p = 0; p < m; ++p) {
			result[p] = new Vector2(0, 0);
			for (var k = 0; k < m; ++k) {
				var d = (zre[k] * Mathf.Cos((2 * Mathf.PI * k * p) / m)) - (zim[k] * Mathf.Sin((2 * Mathf.PI * k * p) / m));
				d *= 1.0f / tr[k];
				result[p] += d;
			}
			result[p] /= m;
		}
		
		return result;
	}
	
	/// <summary>
	/// Обеспечивает периодичность для заданного множества.
	/// </summary>
	/// <param name="j">Индекс элемента.</param>
	/// <param name="N">Количество элементов.</param>
	/// <returns>Периодический индекс элемента.</returns>
	private static int GetPositiveIndex(int j, int N) {
		if (j >= 0) {
			return j % N;
		}
		
		return N - 1 + ((j + 1) % N);
	}
	void OnDrawGizmos()
	{   var waypoints = GetWaypoints ();
		var vectors = new Vector2[waypoints.Length];
		Debug.Log (vectors.Length);
		for(var d=0; d<waypoints.Length; d++)
		{			
			vectors[d].x=waypoints[d] .x;
			vectors[d].y=waypoints[d] .z;			
		}
		
		var spline =  Calculate(vectors, 2, 5, true);
		 
		
		var vectors3 = new Vector3[spline.Length];


		var i = 1;
		for (i=1; i<vectors3.Length; i++) {
						 
			Gizmos.DrawLine(new Vector3(spline[i-1].x,transform.position.y, spline[i-1].y),
			                new Vector3(spline[i].x,transform.position.y, spline[i].y));
		}
		Gizmos.DrawLine(new Vector3(spline[i-1].x,transform.position.y, spline[i-1].y),
		                new Vector3(spline[0].x,transform.position.y, spline[0].y));
	}
	Vector3[] GetWaypoints () {
		// Now, this function basically takes the container object for the waypoints, then finds all of the transforms in it,
		// once it has the transforms, it checks to make sure it's not the container, and adds them to the array of waypoints.
		var potentialWaypoints   = waypointContainer.GetComponentsInChildren< Transform>();
		List<Vector3> waypoints = new  List<Vector3>();
		int i = 0;
		foreach ( Transform potentialWaypoint   in potentialWaypoints ) {
			if ( potentialWaypoint.position != waypointContainer.transform.position) {
				waypoints.Add( potentialWaypoint.transform.position);
				i++;
			}
		}
		return waypoints.ToArray();
	}
}
