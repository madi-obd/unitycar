﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

public class RoadSystem : MonoBehaviour {

	public List<Road> roads = null;
	public List<Intersection> junctions = null;
	
	public RoadSystem()
	{
		roads = new List<Road>();
		junctions = new List<Intersection> ();

	}
	// Use this for initialization
	void Start () {
//		TestPolyLine pl = gameObject.AddComponent<TestPolyLine>();
//		pl.positions=new Vector3[2];
//		pl.positions[0]= this.transform.position;
//		pl.positions[1]=  this.transform.position+new Vector3(10,0,10);
//
//		this.roads.Add(pl);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

   public  Graph getGraph()
    {
        Graph gr = new Graph();
        
        for (int i = 0; i < roads.Count; i++)
        {

            Dictionary<String, double> income = new Dictionary<String, double>();
            Dictionary<String, double> outcome = new Dictionary<String, double>();

            income.Add(roads[i].GetInstanceID() + "B", roads[i].getLine(1).getLineLength());

            for (int j = 0; j < junctions.Count; j++)
            {
                Intersection intersecton = junctions[j];
                foreach (var spline in intersecton.splineLines)
                {
                    if (spline.BeginRoad.GetInstanceID() == roads[i].GetInstanceID())
                    {
                        

                        outcome.Add(spline.EndRoad.GetInstanceID() + "A", spline.getLineLength());
                    }

                   
                }


            }


            gr.add_vertex(roads[i].GetInstanceID()+"A", income);
            gr.add_vertex(roads[i].GetInstanceID() + "B", outcome);

        }
        


         

      //  path.ForEach(x => Debug.Log(x));
        return gr;
    }


    //public SplineLine getSplineByRoads(String idIn, String idOut)
    //{
        
        

    //}

    public Road getRoad(String Id)
    {

        for (int i = 0; i < roads.Count; i++)
        {
            if(roads[i].GetInstanceID() ==Int32.Parse(Id))
            {

                return roads[i];
            }
        }
        return null;
    }

    public Transform[] getSplinePath(String start, String end)
    {
        Graph gr = getGraph();
        List<String> path = gr.shortest_path(start, end);

        path.Reverse();
        List<String> roadsmy = path.Where(x => x.Contains("B")).ToList();

        for (int i = 0; i<roadsmy.Count; i++)
        {
            roadsmy [i]= roadsmy[i].Substring(0, roadsmy[i].Length-1);
        }


        List<Road> roasdInstances = new List<Road>();
        for (int i = 0; i < roadsmy.Count; i++)
        {
            roasdInstances.Add(getRoad(roadsmy[i]));
        }

        int TransformLengtn = 0;

        foreach (var ro in roasdInstances)
        {
            TransformLengtn += ro.positions.Length;
        }


       List< Transform> result = new List<Transform>();






        for (var i = 0; i < roasdInstances.Count; i++)
        {
            Vector3[] line = roasdInstances[i].GetTransformedPositions();
            for (var j = 0; j < line.Length; j++)
            {
                Transform t = new GameObject().transform;
                t.position = new Vector3(line[j].x, line[j].y, line[j].z);
                result.Add(t);
            }
            
             

        }

        return result.ToArray();
    }




   public  void DeleteRoad(Road road)
    {

        for (int i = 0; i < roads.Count; i++)
        {
            if (roads[i].GetInstanceID() == road.GetInstanceID())
            {
                roads.RemoveAt(i);
            }
        }


    }

    public void DeleteJunction(Intersection intersection)
    {

        for (int i = 0; i < junctions.Count; i++)
        {
            if (junctions[i].GetInstanceID() == intersection.GetInstanceID())
            {
                junctions.RemoveAt(i);
            }
        }


    }
}

