//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.18444
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

 
public class Junction: MonoBehaviour, IRoadConnection
	{

	    public Vector3 position = new Vector3();

		public TrafficLight trafficLight = null;

		public List< TestPolyLine > roads = null;

     	public List< int> ConnectIndixes = null;


	   public List <SplineLine> SplineLines = null;


		public Junction ()
		{
			trafficLight = new TrafficLight ();
		    roads = new List<TestPolyLine> ();
		    ConnectIndixes = new List<int> ();
		    SplineLines = new List<SplineLine> ();
		}
	
	void OnDrawGizmos() {
		Vector3 tr = position;
		Gizmos.color = Color.yellow;
		
		Gizmos.DrawCube (tr, new Vector3(20,20,20));

		for (int i =0; i<roads.Count; i++) {

			Gizmos.DrawLine( tr, roads[i].mainTransform.TransformPoint( roads[i].positions[ConnectIndixes[i]]) );
		}
			 

	}
	

}


