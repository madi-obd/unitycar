﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public class RoadNode : IRoadNode
{
    private Guid _Id;
    private IRoad _myRoad;
    private List<ILine> _IncomingLines;
    private List<ILine> _OutgoingLines;
    public Guid Id
    {
        get { return _Id; }

        set { this._Id = value; }
    }

    public List<ILine> IncomingLines
    {
        get
        {
            return this._IncomingLines;
        }

        set { this._IncomingLines = value; }
    }

    public IRoad Onroad
    {
        get { return _myRoad; }

        set { this._myRoad = value; }
    }

    public List<ILine> OutgoingLines
    {
        get { return this._OutgoingLines; }

        set { this._OutgoingLines = value; }
    }
}
 
