﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Line : MonoBehaviour, ILine
{
	public Vector3[] positions ;
	public int LineNumber = 1;
	public float LineWidth = 3.0f;
	IRoad road;
	Guid lineId;

	public Guid getLineId()
	{

		return LineId;
	}

	public Vector3[] getPositions(){

		return positions;
	}
	List<ILine> nextLines;
	List<ILine> prevLines;

    public Line()
    {
        
        positions = new Vector3[2];

    }

    public Guid LineId
    {
        get
        {
            return lineId;
        }

        set
        {
            lineId = value;
        }
    }

    public IRoad getRoad()
	{

		return road;
	}
	public void setRoad(IRoad road)
	{
		this.road = road;
	}
	public int 	getLinePos()
	{

		return LineNumber;
	}
	public void 	setLinePos(int pos)
	{

		this.LineNumber = pos;
	}
	public void 	setLineWidth(float width)
	{

		this.LineWidth = width;
	}
	public float 	getLineWidth()
	{
		return LineWidth;
	}
	public List<ILine> getNextLines()
	{
		return this.nextLines;
	}
	public List<ILine> getPrevLines()
	{
		return this.prevLines;

	}
	public void 	AddNextLine(ILine line)
	{

		this.nextLines.Add (line);
	}
	public void 	AddPrevLine(ILine line)
	{

		this.prevLines.Add (line);
	}
	public void 	DeleteNextLine(ILine line)
	{
		for (int i = 0; i < nextLines.Count; i++) {
			if (nextLines [i].getLineId() == line.getLineId ()) {
				nextLines.RemoveAt (i);
			}
		}

	}
	public void 	DeletePrevLine(ILine line)
	{
		for (int i = 0; i < prevLines.Count; i++) {
			if (prevLines [i].getLineId() == line.getLineId ()) {
				prevLines.RemoveAt (i);
			}
		}

	}



	
	// Use this for initialization
	void Start () {
		positions = new Vector3[0];
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public double getLineLength()
    {
        double length = 0;
        for (int i = 0; i<positions.Length-1; i++ )
        {
            length += (positions[i] - positions[i + 1]).magnitude;
        }
        return length;
    }

    //	Vector3[] GetLine(int number)
    //	{
    //		if (Mathf.Abs (number) > LineNumber) {
    //			return null;
    //		}
    //		if (positions.Length <= 1) {
    //			return null;
    //		}
    //		if (positions.Length == 2) {
    //			Vector3 roadDirection = (positions [0] - positions [1]);
    //			Vector3 roadDirectionPerpendicular = new Vector3 (roadDirection.z, roadDirection.y, -roadDirection.x);
    //
    //			roadDirectionPerpendicular = roadDirectionPerpendicular;
    //			roadDirectionPerpendicular.Normalize();
    //
    //			foreach (Vector3 v  in positions) {
    //				yield return v + roadDirectionPerpendicular * number;
    //
    //			}
    //
    //		} else {
    //
    //			Vector3 A = (positions[0]-positions[1]);
    //			A.Normalize();
    //			Vector3 B = (positions[2]-positions[1]);
    //			B.Normalize();
    //			Vector3 Bissector = new Vector3((A.x+B.x)/2, A.y,(A.z+B.z)/2);
    //			Bissector.Normalize();
    //			foreach (Vector3 v  in positions) {
    //				yield return v + Bissector * number;
    //				
    //			}
    //
    //
    //		}
    //
    //	}




}
