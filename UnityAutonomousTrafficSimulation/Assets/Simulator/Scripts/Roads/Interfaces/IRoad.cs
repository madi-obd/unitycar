﻿using UnityEngine;

public interface IRoad
{
    int LineNumber { get; set; }
    System.Collections.Generic.List<ILine> Lines { get; set; }
    float LineWidth { get; set; }
    Transform MainTransform { get; set; }
    Vector3[] Positions { get; set; }

    IRoadNode getBeginNode();
    IRoadNode getEndNode();
    Vector3[] GetLine(int number);
    ILine getLine(int index);
    Vector3[] GetLine(int number, Vector3[] globalPos);
    System.Collections.Generic.List<ILine> getLines();
    Vector3[] GetTransformedPositions();
    
    void setBeginNode(IRoadNode node);
    void setEndNode(IRoadNode node);
   
}