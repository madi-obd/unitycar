﻿using System;
using System.Collections.Generic;

public interface IRoadNode
{
    Guid Id { get; set; }
    IRoad Onroad { get; set; }
    List<ILine> IncomingLines { get; set; }
    List<ILine> OutgoingLines { get; set; }
}