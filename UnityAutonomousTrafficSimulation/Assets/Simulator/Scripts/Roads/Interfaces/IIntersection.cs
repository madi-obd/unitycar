﻿using System.Collections.Generic;
using UnityEngine;

public interface IIntersection
{
    
    void addIntersection(ILine intersectionLine);
    void addRoad(IRoad line, int pointIndex);
    void deleteIntersection(ILine intersectionLine);
    void deleteRoad(IRoad line);
    void DeleteSpline(SplineLine spline);
    IntersectionLineBundle getBundleByRoad(IRoad road);
    List<ILine> getIntersectons();
    List<Road> getRoads();
}