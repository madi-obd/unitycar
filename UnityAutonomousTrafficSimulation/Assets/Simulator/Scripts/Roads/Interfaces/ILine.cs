﻿using System;
using System.Collections.Generic;
using UnityEngine;

public interface ILine
{
    void AddNextLine(ILine line);
    void AddPrevLine(ILine line);
    void DeleteNextLine(ILine line);
    void DeletePrevLine(ILine line);
    Guid getLineId();
    int getLinePos();
    float getLineWidth();
    List<ILine> getNextLines();
    Vector3[] getPositions();
    List<ILine> getPrevLines();
    IRoad getRoad();
    void setLinePos(int pos);
    void setLineWidth(float width);
    void setRoad(IRoad road);

    double getLineLength();

}