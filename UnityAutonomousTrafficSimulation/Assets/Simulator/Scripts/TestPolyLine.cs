﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TestPolyLine:MonoBehaviour {
	public Vector3[] positions =null ;
	public int LineNumber = 1;
	public float LineWidth = 3.0f;
	public Transform mainTransform = null;
	// Use this for initialization
	void Start () {
		//positions = new Vector3[0];
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnDrawGizmos() {
		Transform tr = mainTransform;
		Gizmos.color = Color.yellow;
		
		for(int line = -LineNumber; line<=LineNumber; line++){
			
			List<Vector3> transformedPos = new List<Vector3>();
			for (int i =0; i<positions.Length; i++) {				
				transformedPos.Add( tr.TransformPoint(positions[i]) );				 
			}
			if(transformedPos.Count>0){			 
				Vector3[] Points = GetLine(line, transformedPos.ToArray()).ToArray();
				for (int i =0; i<Points.Length-1; i++) {
					
					
					Gizmos.DrawLine (   Points[i] ,   Points[i+1]);
				}
			}
		}
	}
	
	
	public List<Vector3> GetLine(int number, Vector3[] globalPos)
	{
		
		List<Vector3> retList = new List<Vector3> ();
		if (Mathf.Abs (number) > LineNumber) {
			return null;
		}
		if (globalPos.Length <= 1) {
			return null;
		}
		if (globalPos.Length == 2) {
			Vector3 roadDirection = (globalPos [0] - globalPos [1]);
			Vector3 roadDirectionPerpendicular = new Vector3 (roadDirection.z, roadDirection.y, -roadDirection.x);
			
			
			roadDirectionPerpendicular.Normalize ();
			
			foreach (Vector3 v  in globalPos) {
				retList.Add( v + roadDirectionPerpendicular.normalized * number);
				
			}
			
		} else {
			Vector3 roadDirection = (globalPos [0] - globalPos [1]);
			Vector3 roadDirectionPerpendicular = new Vector3 (roadDirection.z, roadDirection.y, -roadDirection.x);
			
			
			roadDirectionPerpendicular.Normalize ();
			
			
			retList.Add( globalPos [0] + roadDirectionPerpendicular.normalized * number*LineWidth);
			
			
			
			for (int i =1; i<globalPos.Length-1; i++) {
				
				Vector3 A = (globalPos [i-1] - globalPos [i]);
				A=A.normalized;
				Vector3 B = (globalPos [i+1] - globalPos [i]);
				B=B.normalized;
				Vector3 Bissector = new Vector3 ((A.x + B.x) / 2, A.y, (A.z + B.z) / 2);
				Bissector.Normalize ();
				
				if(Bissector.magnitude==0.0f){
					Bissector=new Vector3((A.z),A.y, -A.x);
				}
				Bissector=Bissector.normalized;
				
				
				if(number<0)
				{
					Vector3 vk=  globalPos[i] +  Bissector.normalized*number*LineWidth ;
					
					if(Mathf.Sign( ((globalPos[i].x - globalPos[i-1].x) * (vk.z - globalPos[i-1].z) - (globalPos[i].z - globalPos[i-1].z) * (vk.x - globalPos[i-1].x)))>0){
						vk = globalPos[i] -  Bissector.normalized*number*LineWidth;
					}
					
					
					retList.Add( vk);
				}
				else{
					Vector3 vk=  globalPos[i] -  Bissector.normalized*number*LineWidth ;
					
					if(Mathf.Sign( ((globalPos[i].x - globalPos[i-1].x) * (vk.z - globalPos[i-1].z) - (globalPos[i].z - globalPos[i-1].z) * (vk.x - globalPos[i-1].x)))<0){
						vk = globalPos[i] + Bissector.normalized*number*LineWidth;
					}

					retList.Add( vk);
				}
				
			}
			
			roadDirection = (globalPos [globalPos.Length-1] - globalPos [globalPos.Length-2]);
			roadDirectionPerpendicular = new Vector3 (roadDirection.z, roadDirection.y, -roadDirection.x);
			
			
			roadDirectionPerpendicular.Normalize ();
			
			
			retList.Add( globalPos [globalPos.Length-1] - roadDirectionPerpendicular.normalized * number*LineWidth);
			
		}
		return retList;
	}
}